
// --- Day 5: A Maze of Twisty Trampolines, All Alike ---

// An urgent interrupt arrives from the CPU: it's trapped in a maze of jump instructions,
// and it would like assistance from any programs with spare cycles to help find the exit.

// The message includes a list of the offsets for each jump.
// Jumps are relative: -1 moves to the previous instruction,
// and 2 skips the next one.
// Start at the first instruction in the list.
// The goal is to follow the jumps until one leads outside the list.

// In addition, these instructions are a little strange; after each jump,
// the offset of that instruction increases by 1. So, if you come across an offset of 3,
// you would move three instructions forrward, but change it to a 4 for the next time it is encountered.

// For example, consider the following list of jump offsets:

// 0
// 3
// 0
// 1
// -3
// Positive jumps ("forward") move downward; negative jumps move upward.
// For legibility in this example, these offset values will be written all on one line,
// with the current instruction marked in parentheses. The following steps would be taken before an exit is found:

// (0) 3  0  1  -3  - before we have taken any steps.
// (1) 3  0  1  -3  - jump with offset 0 (that is, don't jump at all). Fortunately, the instruction is then incremented to 1.
//  2 (3) 0  1  -3  - step forward because of the instruction we just modified. The first instruction is incremented again, now to 2.
//  2  4  0  1 (-3) - jump all the way to the end; leave a 4 behind.
//  2 (4) 0  1  -2  - go back to where we just were; increment -3 to -2.
//  2  5  0  1  -2  - jump 4 steps forward, escaping the maze.
// In this example, the exit is reached in 5 steps.

// How many steps does it take to reach the exit?

struct Cpu {
    counter: u32,
    ptr: usize,
}

impl Cpu {
    fn new() -> Cpu {
        Cpu {
            counter: 0,
            ptr: 0,
        }
    }


    fn run(&mut self, ins_list: &mut [i32]) {
        while self.ptr < ins_list.len() {
            self.counter += 1;

            // fetch instruction.
            let ins = ins_list[self.ptr];

            // increment instruction.
            ins_list[self.ptr] = ins + 1;

            // advance ptr
            let new_ptr = (self.ptr as i32) + ins;
            self.ptr = new_ptr as usize;
        }

    }
}


pub fn solve(ins_str: &str) -> u32 {
    let mut instructions = ins_str
        .split("\n")
        .filter_map(|s| s.parse::<i32>().ok())
        .collect::<Vec<i32>>();

    let mut cpu = Cpu::new();
    cpu.run(&mut instructions);
    cpu.counter

}



#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_example() {
        let test_str = r#"
0
3
0
1
-3"#;

        assert_eq!(5, solve(test_str));
    }
}


