// --- Day 2: Corruption Checksum ---

// As you walk through the door, a glowing humanoid shape yells in your direction.
// "You there! Your state appears to be idle.
// Come help us repair the corruption in this spreadsheet - if we take another millisecond,
// we'll have to display an hourglass cursor!"

// The spreadsheet consists of rows of apparently-random numbers.
// To make sure the recovery process is on the right track, // they need you to calculate the spreadsheet's checksum.
// For each row, determine the difference between the largest value and the smallest value;
// the checksum is the sum of all of these differences.

// For example, given the following spreadsheet:

// 5 1 9 5
// 7 5 3
// 2 4 6 8
// The first row's largest and smallest values are 9 and 1, and their difference is 8.
// The second row's largest and smallest values are 7 and 3, and their difference is 4.
// The third row's difference is 6.
// In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

// What is the checksum for the spreadsheet in your puzzle input?


pub fn solve(input: &str) -> i32 {
    let mut sum: i32 = 0;
    for line in input.lines() {


        let mut smallest = i32::max_value();
        let mut biggest: i32 = 0;

        for word in line.split_whitespace() {
            match word.parse::<i32>() {
                Ok(n) => {

                    if n < smallest {
                        smallest = n;
                    }

                    if biggest < n {
                        biggest = n;
                    }
                },

                Err(e) =>
                    panic!(format!("Failed to parse token \"{}\" as digit: {}", word, e)),
            }
        }

        if smallest != i32::max_value() {
            sum += biggest - smallest;
        }

    }

    sum
}


#[cfg(test)]
mod tests {
    use super::solve;
    static INPUT_STR: &'static str = r"
5 1 9 5
7 5 3
2 4 6 8
";


    #[test]
    fn test_given_input() {
        println!("");
        assert_eq!(solve(INPUT_STR), 18);
    }
}



