

pub trait AsciiDigit {
    fn as_digit(self) -> usize;
}

impl<'a> AsciiDigit for &'a u8 {

    fn as_digit(self) -> usize {
        match self {
            &n if 47 < n && n < 58 => (n - 48) as usize,

            _ =>
                panic!(format!("Cannot convert non digit '{}' to digit", self)),
        }
    }
}

impl<'a> AsciiDigit for &'a char {

    fn as_digit(self) -> usize {
        match self {
            &n if '0' <= n && n <= '9' => (n as usize - 48),

            _ =>
                panic!(format!("Cannot convert non digit '{}' to digit", self)),
        }
    }
}

