

extern crate aoc2017;

static INPUT: &'static str = include_str!("input_day07.txt");

fn main() {
    println!("{:?}", aoc2017::day07::solve(INPUT));
}
