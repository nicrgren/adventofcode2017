

extern crate aoc2017;

static INPUT: &'static str = "197,97,204,108,1,29,5,71,0,50,2,255,248,78,254,63";


fn main() {
    let res = aoc2017::day10::solve(256, INPUT);
    println!("{}", res);
}
