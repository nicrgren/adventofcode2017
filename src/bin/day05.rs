
extern crate aoc2017;


static INPUT: &'static str = include_str!("input_day05.txt");

fn main() {

    let res = aoc2017::day05::solve(INPUT);

    println!("{}", res);

}

