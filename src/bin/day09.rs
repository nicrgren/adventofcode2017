
extern crate aoc2017;


static INPUT: &'static str = include_str!("input_day09.txt");

fn main() {
    let res = aoc2017::day09::solve(INPUT);
    println!("{}", res);

}
