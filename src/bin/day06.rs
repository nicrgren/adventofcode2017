

extern crate aoc2017;

static INPUT: &'static str = "11	11	13	7	0	15	5	5	4	4	1	1	7	1	15	11";
 


fn main() {

    let mut nums: Vec<u32> = INPUT
        .split("\t")
        .filter_map(|s| s.trim().parse().ok())
        .collect();

    println!("Parsed to numbers: {:?}", nums);

    let solution = aoc2017::day06::Memory::from(&mut nums).redistribute();

    println!("{}", solution);


}
