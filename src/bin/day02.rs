extern crate aoc2017;

use aoc2017::day02;


fn main() {
    let input = include_str!("input_day02.txt");

    let res = day02::solve(input);

    println!("day02 res: {}", res);

}
