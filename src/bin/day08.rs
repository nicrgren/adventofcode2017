
extern crate aoc2017;

static INPUT: &'static str = include_str!("input_day08.txt");

fn main() {
    let res = aoc2017::day08::solve(INPUT);


    println!("{:?}", res);

}
