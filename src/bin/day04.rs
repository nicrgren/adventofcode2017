
extern crate aoc2017;


static INPUT: &'static str = include_str!("input_day04.txt");


fn main() {
    println!("No valid passwords: {}",
             aoc2017::day04::count_valid_passwords(INPUT.lines()));
}
