extern crate aoc2017;



static INPUT: &'static str = include_str!("input_day11.txt");

fn main() {
    let res = aoc2017::day11::solve(INPUT);
    println!("{}", res);

}
