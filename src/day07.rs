

// --- Day 7: Recursive Circus ---

// Wandering further through the circuits of the computer,
// you come upon a tower of programs that have gotten themselves
// into a bit of trouble.
// A recursive algorithm has gotten out of hand,
// and now they're balanced precariously in a large tower.

// One program at the bottom supports the entire tower.
// It's holding a large disc, and on the disc are balanced
// several more sub-towers.
// At the bottom of these sub-towers, standing on the bottom disc,
// are other programs, each holding their own disc, and so on.
// At the very tops of these sub-sub-sub-...-towers,
// many programs stand simply keeping the disc below them balanced
// but with no disc of their own.

// You offer to help, but first you need to understand the structure of these towers. You ask each program to yell out their name, their weight, and (if they're holding a disc) the names of the programs immediately above them balancing on that disc. You write this information down (your puzzle input). Unfortunately, in their panic, they don't do this in an orderly fashion; by the time you're done, you're not sure which program gave which information.

// For example, if your list is the following:

// pbga (66)
// xhth (57)
// ebii (61)
// havc (66)
// ktlj (57)
// fwft (72) -> ktlj, cntj, xhth
// qoyq (66)
// padx (45) -> pbga, havc, qoyq
// tknk (41) -> ugml, padx, fwft
// jptl (61)
// ugml (68) -> gyxo, ebii, jptl
// gyxo (61)
// cntj (57)
// ...then you would be able to recreate the structure of the towers that looks like this:

//                 gyxo
//               /
//          ugml - ebii
//        /      \
//       |         jptl
//       |
//       |         pbga
//      /        /
// tknk --- padx - havc
//      \        \
//       |         qoyq
//       |
//       |         ktlj
//        \      /
//          fwft - cntj
//               \
//                 xhth
// In this example, tknk is at the bottom of the tower
// (the bottom program), and is holding up ugml, padx, and fwft.
// Those programs are, in turn, holding up other programs;
// in this example, none of those programs are holding up
// any other programs, and are all the tops of their own towers.
// (The actual tower balancing in front of you is much larger.)

// Before you're ready to help them,
// you need to make sure your information is correct.
// What is the name of the bottom program?


use std::collections::HashMap;

#[derive(Debug, PartialEq)]
pub struct Program<'a> {
    name: &'a str,
    weight: u32,
    children: Vec<&'a str>,
}


pub fn solve<'a>(s: &'a str) -> Vec<&'a str> {
    let bs: &[u8] = s.as_bytes();
    let program_list: Vec<Program> = parse::programs(bs);
    println!("Found {} programs", program_list.len());

    let mut map: HashMap<&str, usize> = HashMap::new();

    for p in program_list {
        map.entry(p.name).or_insert(0);

        for child in p.children {
            *map.entry(child).or_insert(0) += 1;
        }

    }



    map.iter()
        .filter(|&(_, &v)| v == 0)
        .map(|(&k, _)| k)
        .collect::<Vec<&str>>()

}



mod parse {

    use nom::*;
    use std::str;
    use std::str::FromStr;
    use super::Program;


    pub fn programs(bs: &[u8]) -> Vec<Program> {
        program_list(bs).unwrap().1
    }


    named!(program( &[u8] ) -> Program,
       do_parse!(
           name: name  >>
           take!(1) >>
           weight: parenthesised_u32 >>
           children: child_list >>

          (Program {
              name: name,
              weight: weight,
              children: children,
          })
       )
    );



    #[test]
    fn test_parse_program() {
        use nom::IResult::Done;

        assert_eq!(
            Done(
                &b""[..],
                Program {
                    name: "someprogram",
                    weight: 32,
                    children: vec!["child1", "child2", "child3"],
                },
            ),
            program(b"someprogram (32) -> child1, child2, child3")
        );
    }



    named!(program_list( &[u8] ) -> Vec<Program>,
       many0!(
           do_parse!(res: program >>
                     alt!(line_ending | eof!()) >>
                     (res)
       ))
    );

    #[test]
    fn test_parse_program_list() {
        use nom::IResult::Done;

        let s = br#"firstprogram (21) -> firstchild, secondChild, thirdchild
secondProgram (32) -> fourthChild, fifthChild
"#;
        assert_eq!(
            Done(
                &b""[..],
                vec![
                    Program {
                        name: "firstprogram",
                        weight: 21,
                        children: vec!["firstchild", "secondChild", "thirdchild"],
                    },
                    Program {
                        name: "secondProgram",
                        weight: 32,
                        children: vec!["fourthChild", "fifthChild"],
                    },
                ],
            ),
            program_list(s)
        );
    }



    named!(child_list ( &[u8] ) -> Vec<&str> ,
       many0!(
           do_parse!(alt!(tag!(" -> ") | tag!(", ")) >>
                     name: map_res!(alphanumeric, str::from_utf8) >>
                     (name)
           )
       )
    );

    #[test]
    fn test_parse_childs() {
        use nom::IResult::Done;

        assert_eq!(Done(&b""[..], vec!["child"]), child_list(b" -> child"));

        assert_eq!(
            Done(&b""[..], vec!["child1", "child2"]),
            child_list(b" -> child1, child2")
        );
    }



    named!(parenthesised_u32( &[u8] ) -> u32,
       map_res!(
           map_res!(
               delimited!(char!('('), is_not!(")"), char!(')')),
               str::from_utf8),
           u32::from_str)
    );

    #[test]
    fn test_parse_parenthesised_u32() {
        use nom::IResult::Done;
        assert_eq!(Done(&b""[..], (23)), parenthesised_u32(b"(23)"));
    }



    named!(name( &[u8] ) -> &str,
       map_res!(alpha,
                str::from_utf8)
    );

    #[test]
    fn test_parse_name() {
        use nom::IResult::Done;
        assert_eq!(Done(&b" "[..], ("somecoolname")), name(b"somecoolname "));
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_example() {
        let test_str = r#"pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)
"#;

        assert_eq!(vec!["tknk"], solve(test_str));


    }

}
