
use nom::*;
use failure::Error;
use std::str::FromStr;
use std::str;
use std::fmt;


pub fn solve(s: &str) -> u32 {

    let instructions = match parse_directions(s.as_bytes()) {
        IResult::Done(_, dirs) => dirs,
        IResult::Incomplete(n) => panic!(format!("Needed {:?} more bytes", n)),
        IResult::Error(e) => panic!(format!("Error parsing instructions: {}", e)),
    };

    let mut p = Position::new();
    p.move_many(&instructions);


    // for every change in x, we move once correctly in y.
    // so subtract each odd y move and move twice for the rest.

    println!("abs (x, y) = ({}, {})", p.x, p.y);


    p.distance_to_origo()

}


#[derive(Debug, PartialEq)]
struct Position {
    x: i32,
    y: i32,
}

impl fmt::Display for Position {


    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }

}
impl Position {

    fn new() -> Position {
        Position {
            x: 0,
            y: 0,
        }
    }

    fn move_in(&mut self, d: &Direction) {
        use self::Direction::*;
        match *d {
            N => self.y += 2,
            S => self.y -= 2,
            NE => {
                self.x += 1;
                self.y += 1;
            },
            SE => {
                self.y -= 1;
                self.x += 1;
            },
            SW => {
                self.y -= 1;
                self.x -= 1;
            },
            NW => {
                self.y += 1;
                self.x -= 1;
            },
        }
    }

    fn move_many(&mut self, directions: &[Direction]) {
        for d in directions {
            self.move_in(d)
        }
    }


    fn distance_to_origo(&self) -> u32 {
        let mut moves: u32 = 0;
        let (x, y) = (self.x.abs(), self.y.abs());

        // always have to walk in x.
        moves += x as u32;

        let y_rest = y - x;

        if 0 < y_rest {
            moves += (y_rest / 2) as u32;
        }

        moves
    }
}


#[derive(Debug, PartialEq, Clone)]
enum Direction {
    N,
    NE,
    SE,
    S,
    SW,
    NW,
}

impl FromStr for Direction {
    type Err = Error;

    fn from_str(s: &str) -> Result<Direction, Self::Err> {
        use self::Direction::*;
        match s {
            "n" => Ok(N),
            "ne" => Ok(NE),
            "se" => Ok(SE),
            "s" => Ok(S),
            "sw" => Ok(SW),
            "nw" => Ok(NW),

            s => Err(format_err!("{} is not a valid direction", s)),
        }
    }
}

named!(parse_directions (&[u8]) -> Vec<Direction> ,
       many0!(
           do_parse!(
               dir: map_res!(map_res!(alpha, str::from_utf8), Direction::from_str) >>
               is_a!(&b","[..]) >>
               (dir)
           )
       )
);



#[cfg(test)]
mod tests {

    use nom::IResult;
    use super::*;
    use super::Direction::*;

    #[test]
    fn test_calc_length_back() {
        let tests = vec![
            (vec![N,N,N,N], 4),
            (vec![N,S,N,S], 0),
            (vec![NE,NE,NE], 3),
            (vec![NE,NE,SW,SW], 0),
            (vec![NE,NE,S,S], 2),
            (vec![SE,SW,SE,SW,SW], 3),
        ];


        for (instructions, expected) in tests {
            let mut p = Position::new();
            p.move_many(&instructions);

            assert_eq!(
                p.distance_to_origo(),
                expected,
                "failed: {:?}", instructions
            );
        }
    }

    #[test]
    fn test_simple_move() {

        let tests = vec![
            (vec![N,N,N,N], Position { x: 0, y: 8 }),
            (vec![N,S,N,S], Position { x: 0, y: 0 }),
            (vec![N,SE,SE,NE], Position { x: 3, y: 1 }),
            (vec![S,S,SE,SE], Position { x: 2, y: -6 }),
            (vec![SW,SW,S,S], Position { x: -2, y: -6 }),
        ];

        for (ref instructions, ref expected) in tests {
            let mut p = Position::new();

            for ins in instructions {
                p.move_in(ins);
            }

            assert_eq!(&p, expected,
                       "{:?} failed. Expected: {}, got: {}",
                       instructions, expected, p);

        }

    }


    #[test]
    fn test_parse_simple_instructions() {
        let tests = vec![
            (&b"ne,ne,ne"[..], vec![NE, NE, NE]),
            (&b"ne"[..], vec![NE]),
            (&b"n,s,sw"[..], vec![N, S, SW]),
            (&b"s"[..], vec![S]),
            (&b"s,nw,s"[..], vec![S, NW, S]),
        ];

        for test in tests {
            assert_eq!(
                parse_directions(test.0),
                IResult::Done(&b""[..], test.1)
            );
        }

    }
}
