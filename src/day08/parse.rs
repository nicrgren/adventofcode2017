
#![allow(dead_code)]

use nom::*;
use failure::Error;
use std::str::FromStr;
use std::str;

use super::{Expression, Condition, DeltaMod, Operator, Operand};


fn operand<'a>(bs: &'a [u8]) -> Result<Operand<'a>, Error> {
    let s = str::from_utf8(bs).map_err(|e| {
        format_err!("Could not convert bytes to str: {}", e)
    })?;


    i32::from_str(s).map(|n| Operand::Constant(n)).or(Ok(
        Operand::Register(s),
    ))
}

fn operator(bs: &[u8]) -> Result<Operator, Error> {
    let s = str::from_utf8(bs)?;
    match s {
        "<" => Ok(Operator::Lt),
        ">" => Ok(Operator::Gt),
        "<=" => Ok(Operator::LtOrEq),
        ">=" => Ok(Operator::GtOrEq),
        "==" => Ok(Operator::Eq),
        "!=" => Ok(Operator::NotEq),
        _ => Err(format_err!("Unknown operator: {}", s)),
    }
}


pub fn expression(bs: &[u8]) -> Result<Expression, Error> {
    use nom::IResult;

    match parse_expression(bs) {
        IResult::Done(_, expr) => Ok(expr),
        IResult::Error(err) => Err(format_err!("{}", err)),
        IResult::Incomplete(_) => Err(format_err!("Incomplete expression")),
    }
}

pub fn expressions(bs: &[u8]) -> Result<Vec<Expression>, Error> {
    use nom::IResult;

    match parse_expression_list(bs) {
        IResult::Done(_, exprs) => Ok(exprs),
        IResult::Error(err) => Err(format_err!("{}", err)),
        IResult::Incomplete(_) => Err(format_err!("Incomplete expression list")),
    }


}

named!(parse_expression_list( &[u8] ) -> Vec<Expression> ,
       many0!(
           do_parse!(opt!(multispace) >>
                     res: parse_expression >>
                     opt!(multispace) >>
                     (res)
           )
       )
);

// "c dec -10 if a >= 1"
named!(parse_expression( &[u8] ) -> Expression ,
       do_parse!(

           // "c"
           reg: map_res!(alpha, str::from_utf8) >>

           many1!(is_a!(&b" "[..])) >>

           // "dec"
           delta_mod: map_res!(map_res!(alt!(tag!("inc") | tag!("dec")),
                                        str::from_utf8),
                               DeltaMod::from_str) >>

           many1!(is_a!(&b" "[..])) >>

           // "-10"
           delta_amt: map_res!(map_res!(is_not!(&b" "[..]),
                                        str::from_utf8),
                               i32::from_str) >>

           many1!(is_a!(&b" "[..])) >>
           tag!("if") >>
           many1!(is_a!(&b" "[..])) >>
           cond: parse_condition >>
           (Expression {
               var: reg,
               delta_mod: delta_mod,
               delta: delta_amt,
               condition: cond,
           })
       )
);

// a >= 1
#[allow(dead_code)]
named!(parse_condition(& [u8]) -> Condition ,
           do_parse!(arg1: map_res!(alt!(alpha | digit), operand) >>
                     many1!(is_a!(&b" "[..])) >>
                     op: map_res!(is_not!(&b" "[..]), operator) >>
                     many1!(is_a!(&b" "[..])) >>
                     arg2: map_res!(is_not!("\n "), operand) >>
                     (Condition(arg1, op, arg2))
           )
    );

#[cfg(test)]
mod tests {
    use super::*;
    use super::Condition;
    use super::Operator::*;
    use super::Operand::*;


    #[test]
    fn test_parse_simple_expressions() {
        // c dec -10 if a >= 1

        let expected = Expression {
            var: "c",
            delta_mod: DeltaMod::Dec,
            delta: -10,
            condition: Condition(Register("a"), GtOrEq, Constant(1)),
        };

        assert_eq!(expected,
                   expression(&b"c dec -10 if a >= 1"[..]).unwrap()
        );


        assert_eq!(
            Expression{
                var: "z",
                delta_mod: DeltaMod::Inc,
                delta: 469,
                condition: Condition(Register("znl"), NotEq, Constant(-9))
            },
            expression(&b"z inc 469 if znl != -9"[..]).unwrap());
    }


    #[test]
    fn test_parse_simple_expression_leave_newline() {
        let expected_expr = Expression {
            var: "a",
            delta_mod: DeltaMod::Dec,
            delta: 10,
            condition: Condition(Register("a"), GtOrEq, Constant(1)),
        };

        let expected = IResult::Done(&b"\n"[..], expected_expr);

        assert_eq!(expected,
                   parse_expression(&b"a dec 10 if a >= 1\n"[..]));
    }


    #[test]
    fn test_parse_fails_with_multispace_before() {
        let res = parse_expression(&b" a dec 10 if a >= 1\n"[..]);
        assert!(res.is_err());

        let res = parse_expression(&b"\na dec 10 if a >= 1\n"[..]);
        assert!(res.is_err());

    }




    #[test]
    fn test_parse_expression_list() {
        let input = r#"
aasd inc 20 if asdb != 1
hhjk dec -30 if gghk == 23
"#;


        let expected = vec![
            Expression{
                var: "aasd",
                delta_mod: DeltaMod::Inc,
                delta: 20,
                condition: Condition(Register("asdb"), NotEq, Constant(1))
            },
            Expression {
                var: "hhjk",
                delta_mod: DeltaMod::Dec,
                delta: -30,
                condition: Condition(Register("gghk"), Eq, Constant(23))
            }
        ];


        assert_eq!(
            expressions(input.as_bytes()).unwrap(),
            expected
        )

    }

    #[test]
    fn test_parse_condition() {
        use nom::IResult::Done;

        assert_eq!(
            Done(&b""[..], Condition(Register("a"), GtOrEq, Constant(32))),
            parse_condition(&b"a >= 32"[..])
        );


        assert_eq!(
            Done(&b""[..], Condition(Register("znl"), NotEq, Constant(-9))),
            parse_condition(&b"znl != -9"[..])
        );

        assert_eq!(
            Done(&b""[..], Condition(Register("a"), GtOrEq, Constant(32))),
            parse_condition(&b"a >= 32"[..])
        );
        assert_eq!(
            Done(
                &b"\n"[..],
                Condition(Register("abcde"), GtOrEq, Constant(32)),
            ),
            parse_condition(&b"abcde >= 32\n"[..])
        );


    }

    #[test]
    fn test_parse_operand() {
        assert_eq!(operand(&b"a"[..]).unwrap(), Register("a"));

        assert_eq!(operand(&b"23"[..]).unwrap(), Constant(23));

    }
}
