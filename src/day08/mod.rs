
use std::str;
use failure::Error;
use std::collections::HashMap;

mod parse;


// --- Day 8: I Heard You Like Registers ---

// You receive a signal directly from the CPU. Because of your recent
// assistance with jump instructions, it would like you to compute
// the result of a series of unusual register instructions.

// Each instruction consists of several parts: the register to modify,
// whether to increase or decrease that register's value,
// the amount by which to increase or decrease it, and a condition.
// If the condition fails, skip the instruction without modifying the register.
// The registers all start at 0. The instructions look like this:

// b inc 5 if a > 1
// a inc 1 if b < 5
// c dec -10 if a >= 1
// c inc -20 if c == 10
// These instructions would be processed as follows:

// Because a starts at 0, it is not greater than 1, and so b is not modified.
// a is increased by 1 (to 1) because b is less than 5 (it is 0).
// c is decreased by -10 (to 10) because a is now greater than or equal to 1 (it is 1).
// c is increased by -20 (to -10) because c is equal to 10.
// After this process, the largest value in any register is 1.

// You might also encounter <= (less than or equal to) or != (not equal to).
// However, the CPU doesn't have the bandwidth to tell you what all the
// registers are named, and leaves that to you to determine.

// What is the largest value in any register after completing the instructions in your puzzle input?


#[derive(Debug, PartialEq)]
pub enum Operand<'a> {
    Register(&'a str),
    Constant(i32),
}

#[derive(Debug, PartialEq)]
pub enum Operator {
    Lt,
    Gt,
    LtOrEq,
    GtOrEq,
    Eq,
    NotEq,
}

impl Operator {

    fn apply(&self, p1: i32, p2: i32) -> bool {
        use self::Operator::*;
        match *self {
            Lt => p1 < p2,
            Gt => p1 > p2,
            LtOrEq => p1 <= p2,
            GtOrEq => p1 >= p2,
            Eq => p1 == p2,
            NotEq => p1 != p2
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Condition<'a>(Operand<'a>, Operator, Operand<'a>);

#[derive(Debug, PartialEq)]
pub enum DeltaMod {
    Inc,
    Dec,
}


impl str::FromStr for DeltaMod {

    type Err = Error;

    fn from_str(s: &str) -> Result<DeltaMod, Error> {
        match s {
            "inc" => Ok(DeltaMod::Inc),
            "dec" => Ok(DeltaMod::Dec),
            s => Err(format_err!("Invalid delta mod: \"{}\"", s)),
        }
    }
}


struct Register<'a>(HashMap<&'a str, i32>);

impl<'a> Register<'a> {

    fn new() -> Register<'a> {
        Register(HashMap::new())
    }

    fn valid(&self, cond: &Condition<'a>) -> bool {

        let lh = match cond.0 {
            Operand::Register(var) =>
                self.0.get(var).map(|&i| i).unwrap_or(0),

            Operand::Constant(i) => i,
        };

        let rh = match cond.2 {
            Operand::Register(var) =>
                self.0.get(var).map(|&i| i).unwrap_or(0),
            Operand::Constant(i) => i,
        };


        cond.1.apply(lh, rh)
    }
}


#[derive(Debug, PartialEq)]
pub struct Expression<'a> {
    var: &'a str,
    delta_mod: DeltaMod,
    delta: i32,
    condition: Condition<'a>,
}

impl<'a> Expression<'a> {

    fn delta_amt(&self) -> i32 {
        match self.delta_mod {
            DeltaMod::Dec => self.delta * -1,
            DeltaMod::Inc => self.delta,
        }
    }
}


pub fn solve<'a>(input: &'a str) -> Result<(&'a str, i32), Error> {
    let expressions = parse::expressions(input.as_bytes())?;

    println!("Parsed {} expressions", expressions.len());
    for expr in expressions.iter() {
        println!("{:?}", expr);
    }
    let mut register = Register::new();

    for expr in expressions {
        if register.valid(&expr.condition) {
            let entry = register.0.entry(expr.var).or_insert(0);
            *entry += expr.delta_amt();
        } else {
            register.0.entry(expr.var).or_insert(0);
        }
    }

    let mut largest_key = "";
    let mut largest = 0;
    for (key, &value) in register.0.iter() {
        if largest < value {
            largest_key = key;
            largest = value;
        }
    }

    Ok((largest_key, largest))
}

