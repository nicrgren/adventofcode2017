

use ::ascii_digit::AsciiDigit;


pub fn solve(s: &str) -> usize {
    let bs: &[u8] = s.as_bytes();
    let mut sum: usize = 0;
    for (f, s) in bs.iter().zip(bs[1..bs.len()].iter()){
        if f == s {
            sum += f.as_digit();
        }
    }


    // check the wrap.
    if bs[0] == bs[bs.len()-1] {
        sum += bs[0].as_digit();
    }

    sum
}





#[cfg(test)]
mod tests {
    use super::solve;
    #[test]
    fn it_works() {
        assert_eq!(solve("1122"), 3);
        assert_eq!(solve("1111"), 4);
        assert_eq!(solve("1234"), 0);
        assert_eq!(solve("91212129"), 9);
    }
}
