// --- Day 3: Spiral Memory ---

// You come across an experimental new kind of memory stored on an infinite two-dimensional grid.

// Each square on the grid is allocated in a spiral pattern starting at a
// location marked 1 and then counting up while spiraling outward.
// For example, the first few squares are allocated like this:

// 37 36  35  34  33  32  31
// 38 17  16  15  14  13  30
// 39 18   5   4   3  12  29
// 40 19   6   1   2  11  28
// 41 20   7   8   9  10  27
// 42 21  22  23  24  25  26
// 43 44  45  46  47  48  49 
// While this is very space-efficient (no squares are skipped),
// requested data must be carried back to square 1
// (the location of the only access port for this memory system)
// by programs that can only move up, down, left, or right.
// They always take the shortest path: the Manhattan Distance between
// the location of the data and square 1.

// For example:

// Data from square 1 is carried 0 steps, since it's at the access port.
// Data from square 12 is carried 3 steps, such as: down, left, left.
// Data from square 23 is carried only 2 steps: up twice.
// Data from square 1024 must be carried 31 steps.
// How many steps are required to carry the data from the square
// identified in your puzzle input all the way to the access port?
// Your puzzle input is 347991.


use std::fmt;

pub fn print_spiral_numbers_to(max: u32) {
    for n in Spinner::to(max) {
        println!("{}", n);
    }
}

pub fn solve() {
    let n = Spinner::to(347991).find(|sn| sn.0 == 347991).unwrap();
    println!("{:?}", n.get_x().abs() + n.get_y().abs());

}


#[derive(Clone, Debug, PartialEq)]
struct Coord(i32, i32);


impl Coord {
    fn new(x: i32, y: i32) -> Coord {
        Coord(x, y)
    }
}

#[derive(Debug, Clone, PartialEq)]
struct SpiralNumber(u32, Coord);

impl SpiralNumber {
    fn get_x(&self) -> i32 {
        (self.1).0
    }

    fn get_y(&self) -> i32 {
        (self.1).1
    }
}

impl fmt::Display for SpiralNumber {

    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let &SpiralNumber(ref n, ref coord) = self;
        write!(f, "{} @ ({}, {})", n, coord.0, coord.1)
    }
}

struct Spinner {
    max_n: u32,
    n: u32,
    pos: Coord,

    width: u32,
    steps_remaining_x: u32,
    steps_remaining_y: u32,

    direction: i8,
}

impl Spinner {
    pub fn to(max: u32) -> Spinner {
        Spinner {
            max_n: max,
            n: 0,
            pos: Coord::new(0, 0),

            width: 0,
            steps_remaining_x: 0,
            steps_remaining_y: 0,

            direction: -1, // start negative, will flip after 1
        }
    }

    fn step(&mut self) {
        self.n += 1;

        if 0 < self.steps_remaining_x {
            self.pos.0 += (1 * self.direction) as i32;
            self.steps_remaining_x -= 1;
        } else if 0 < self.steps_remaining_y {
            self.pos.1 += (1 * self.direction) as i32;
            self.steps_remaining_y -= 1;
        }

        if self.steps_remaining_x == 0 && self.steps_remaining_y == 0 {
            self.width += 1;
            self.steps_remaining_x = self.width;
            self.steps_remaining_y = self.width;
            self.direction *= -1;
        }
    }

    fn current(&self) -> SpiralNumber {
        SpiralNumber(self.n, self.pos.clone())
    }
}



impl Iterator for Spinner {
    type Item = SpiralNumber;

    fn next(&mut self) -> Option<Self::Item> {
        if self.n < self.max_n {
            self.step();
            Some(self.current())
        } else {
            None
        }
    }

}



#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_spinner_to_10() {
        let mut s = Spinner::to(12);
        assert_eq!(s.next(), Some(SpiralNumber(1, Coord(0, 0))));
        assert_eq!(s.next(), Some(SpiralNumber(2, Coord(1, 0))));
        assert_eq!(s.next(), Some(SpiralNumber(3, Coord(1, 1))));
        assert_eq!(s.next(), Some(SpiralNumber(4, Coord(0, 1))));
        assert_eq!(s.next(), Some(SpiralNumber(5, Coord(-1, 1))));
        assert_eq!(s.next(), Some(SpiralNumber(6, Coord(-1, 0))));
        assert_eq!(s.next(), Some(SpiralNumber(7, Coord(-1, -1))));
        assert_eq!(s.next(), Some(SpiralNumber(8, Coord(0, -1))));
        assert_eq!(s.next(), Some(SpiralNumber(9, Coord(1, -1))));
        assert_eq!(s.next(), Some(SpiralNumber(10, Coord(2, -1))));
        assert_eq!(s.next(), Some(SpiralNumber(11, Coord(2, 0))));
        assert_eq!(s.next(), Some(SpiralNumber(12, Coord(2, 1))));
    }

}
