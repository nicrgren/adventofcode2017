


enum Token {
    Open,
    Close,
}

pub fn solve(s: &str) -> u32 {
    let p = Parser::new(s);
    let mut score = 0;
    let mut delta = 1; // inc when open, dec when close;


    for token in p {

        match token {
            Token::Open => {
                // add to score on each open.
                score += delta;
                delta += 1;
            },

            Token::Close => delta -= 1,
        }
    }

    score
}


struct Parser<'a> {
    bytes: &'a [u8],
    i: usize,
}

impl<'a> Parser<'a> {
    fn new(input: &'a str) -> Parser {

        Parser {
            bytes: input.as_bytes(),
            i: 0,
        }
    }
}

impl<'a> Iterator for Parser<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        use self::Token::*;

        let mut ignore_next = false;
        let mut eating_garbage = false;

        for (i, &c) in self.bytes[self.i..].iter().enumerate() {
            if ignore_next {
                ignore_next = false;
                continue;
            }
            match c {
                b'{' if !eating_garbage => {
                    self.i += i + 1;
                    return Some(Open);
                },

                b'}' if !eating_garbage => {
                    self.i += i + 1;
                    return Some(Close);
                },

                b'!' => {
                    ignore_next = true;
                },

                b'<' if !eating_garbage => {
                    eating_garbage = true;
                }

                b'>' if eating_garbage => {
                    eating_garbage = false;
                }

                _ => continue
            }
        }

        None
    }
}



#[cfg(test)]
mod tests {

    use super::*;

    // {}, 1 group.
    // {{{}}}, 3 groups.
    // {{},{}}, also 3 groups.
    // {{{},{},{{}}}}, 6 groups.
    // {<{},{},{{}}>}, 1 group (which itself contains garbage).
    // {<a>,<a>,<a>,<a>}, 1 group.
    // {{<a>},{<a>},{<a>},{<a>}}, 5 groups.
    // {{<!>},{<!>},{<!>},{<a>}}, 2 groups (since all but the last > are canceled).


    #[test]
    fn test_examples_are_valid() {
        let examples = vec![
            "{}",
            "{{{}}}",
            "{{},{}}",
            "{{{},{},{{}}}}",
            "{<{},{},{{}}>}",
            "{<a>,<a>,<a>,<a>}",
            "{{<a>},{<a>},{<a>},{<a>}}",
            "{{<!>},{<!>},{<!>},{<a>}}",
        ];


        for example in examples.iter() {
            let mut p = Parser::new(example);
            let mut open = 0; // count how many open groups we have

            for token in p {
                match token {
                    Token::Close if open == 0 =>
                        assert!(false, "Ran into Close when open was 0"),

                    Token::Open => open += 1,

                    Token::Close => open -= 1,

                }
            }

            assert!(open == 0,
                    "Open was {} after iteration for example: {}",
                    open, example);
        }
    }


    #[test]
    fn test_example_group_count() {
        let examples = vec![
            ("{}", 1),
            ("{{{}}}", 3),
            ("{{},{}}", 3),
            ("{{{},{},{{}}}}", 6),
            ("{<{},{},{{}}>}", 1),
            ("{<a>,<a>,<a>,<a>}", 1),
            ("{{<a>},{<a>},{<a>},{<a>}}", 5),
            ("{{<!>},{<!>},{<!>},{<a>}}", 2),
        ];


        for example in examples.iter() {
            let mut p = Parser::new(example.0);
            let mut no_groups = 0; // count how many open groups we have

            for token in p {
                match token {
                    Token::Open => no_groups += 1,

                    _ => continue,

                }
            }

            assert!(no_groups == example.1,
                    "{}: Expected no groups {}, but was {}",
                    example.0, example.1, no_groups);
        }
    }



    #[test]
    fn test_example_score() {
        let examples = vec![
            ("{}", 1),
            ("{{{}}}", 6),
            ("{{},{}}", 5),
            ("{{{},{},{{}}}}", 16),
            ("{<a>,<a>,<a>,<a>}", 1),
            ("{{<ab>},{<ab>},{<ab>},{<ab>}}", 9),
            ("{{<!!>},{<!!>},{<!!>},{<!!>}}", 9),
            ("{{<a!>},{<a!>},{<a!>},{<ab>}}", 3),
        ];


        for example in examples {
            let score = solve(example.0);

            assert!(score == example.1,
                    "\"{}\": Expected score {}, but got {}",
                    example.0, example.1, score);
        }
    }
}






