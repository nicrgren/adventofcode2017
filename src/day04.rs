

// --- Day 4: High-Entropy Passphrases ---

// A new system policy has been put in place that requires all
// accounts to use a passphrase instead of simply a password.
// A passphrase consists of a series of words (lowercase letters) separated by spaces.

// To ensure security, a valid passphrase must contain no duplicate words.

// For example:
// aa bb cc dd ee is valid.
// aa bb cc dd aa is not valid - the word aa appears more than once.
// aa bb cc dd aaa is valid - aa and aaa count as different words.

// The system's full passphrase list is available as your puzzle input. How many passphrases are valid?


use std::collections::HashMap;



fn is_valid_password(s: &str) -> bool {
    let mut map: HashMap<&str, u8> = HashMap::new();

    for word in s.split_whitespace() {
        let entry = map.entry(word).or_insert(0);
        if 0 < *entry {
            return false;
        }
        *entry += 1;
    }
    true
}


pub fn count_valid_passwords<'a, T: Iterator<Item = &'a str>>(str_iter: T) -> usize {
    str_iter.filter(|line| is_valid_password(line)).count()
}



#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_examples() {
        assert!(is_valid_password("aa bb cc dd ee"));
        assert!(!is_valid_password("aa bb cc dd aa")); 
        assert!(is_valid_password("aa bb cc dd aaa"));
       
    }
}





