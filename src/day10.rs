
use failure::Error;
use nom::*;

use std::str;
use std::str::FromStr;

struct CyclicIndexIter {
    container_len: usize,
    counter: usize,
}


struct BackwardsCyclicIterator {
    container_len: usize,
    counter: usize,
}

impl BackwardsCyclicIterator {

    fn new(start_at: usize, container_len: usize) -> BackwardsCyclicIterator {
        BackwardsCyclicIterator {
            container_len: container_len,
            counter: start_at % container_len,
        }
    }
}

impl Iterator for BackwardsCyclicIterator {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        None
    }
}


pub fn solve(_list_length: u32, _lengths_str: &str) -> u32 {
    0
}

fn rotate_slice(arr: &mut [u32]) {
    let l = arr.len();
    for i in 0..(l / 2) {
        let swap = arr[i];
        arr[i] = arr[l - 1 - i];
        arr[ l - 1 - i] = swap;
    }

}


named!(parse_list (&[u8]) -> Vec<u32> ,
       many1!(
           do_parse!(
               number: map_res!(map_res!(digit, str::from_utf8), u32::from_str) >>
               is_a!(&b","[..]) >>
               (number)
           )
       )
);

pub fn parse_length_list(s: &str) -> Result<Vec<u32>, Error> {
    match parse_list(s.as_bytes()) {

        IResult::Done(_, res) =>
            Ok(res),

        IResult::Error(err) =>
            Err(format_err!("Error parsing list: {}", err)),

        IResult::Incomplete(_) =>
            Err(format_err!("Error parsing list, needed more bytes")),

    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_parser() {
        let input = "1,2,3,4,5,6,7,8";
        let expected = vec![1, 2, 3, 4, 5, 6, 7, 8];

        let res = parse_length_list(input);
        assert!(res.is_ok());

        assert_eq!(res.unwrap(), expected);

    }

    #[test]
    fn test_rotate_slice() {
        let mut slice = [1, 2, 3, 4, 5, 6];
        rotate_slice(&mut slice);
        assert_eq!(
            slice,
            [6, 5, 4, 3, 2, 1]
        );

        let mut slice = [1];
        rotate_slice(&mut slice);
        assert_eq!(
            slice,
            [1]
        );


    }
}
